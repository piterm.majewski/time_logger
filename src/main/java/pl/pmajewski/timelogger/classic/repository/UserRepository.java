package pl.pmajewski.timelogger.classic.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pmajewski.timelogger.classic.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
