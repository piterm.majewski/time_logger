package pl.pmajewski.timelogger.classic.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pmajewski.timelogger.classic.model.Record;

public interface RecordsRepository extends CrudRepository<Record, Long> {
}
