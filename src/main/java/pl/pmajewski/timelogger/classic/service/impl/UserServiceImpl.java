package pl.pmajewski.timelogger.classic.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pmajewski.timelogger.classic.dto.UserCreateDTO;
import pl.pmajewski.timelogger.classic.dto.UserDTO;
import pl.pmajewski.timelogger.classic.dto.UserUpdateDTO;
import pl.pmajewski.timelogger.classic.exception.UserNotFoundException;
import pl.pmajewski.timelogger.classic.model.User;
import pl.pmajewski.timelogger.classic.repository.UserRepository;
import pl.pmajewski.timelogger.classic.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository repository;

    public UserServiceImpl(UserRepository userRepo) {
        this.repository = userRepo;
    }

    @Override
    public UserDTO create(UserCreateDTO body) {
        User user = User.of(body);
        repository.save(user);
        return user.getDTO();
    }

    @Override
    public UserDTO update(UserUpdateDTO body) {
        User user = repository.findById(body.getId()).orElseThrow(UserNotFoundException::new);
        user.update(body);
        return user.getDTO();
    }

    @Override
    public void remove(Long id) {
        User user = repository.findById(id).orElseThrow(UserNotFoundException::new);
        repository.delete(user);
    }
}
