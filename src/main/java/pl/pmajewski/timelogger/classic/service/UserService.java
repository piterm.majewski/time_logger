package pl.pmajewski.timelogger.classic.service;

import pl.pmajewski.timelogger.classic.dto.UserCreateDTO;
import pl.pmajewski.timelogger.classic.dto.UserDTO;
import pl.pmajewski.timelogger.classic.dto.UserUpdateDTO;

public interface UserService {

    UserDTO create(UserCreateDTO body);

    UserDTO update(UserUpdateDTO body);

    void remove(Long id);
}
