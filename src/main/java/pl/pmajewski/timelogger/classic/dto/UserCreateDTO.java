package pl.pmajewski.timelogger.classic.dto;

import lombok.Data;

@Data
public class UserCreateDTO {

    private String email;
    private String password;
}
