package pl.pmajewski.timelogger.classic.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RecordUpdateDTO {

    private Long id;
    private String name;
    private LocalDateTime begin;
    private LocalDateTime end;
}
