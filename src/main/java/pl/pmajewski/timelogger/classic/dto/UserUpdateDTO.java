package pl.pmajewski.timelogger.classic.dto;

import lombok.Data;

@Data
public class UserUpdateDTO {

    private Long id;
    private String email;
    private String password;
}
