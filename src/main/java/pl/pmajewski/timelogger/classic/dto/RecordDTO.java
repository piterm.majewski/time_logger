package pl.pmajewski.timelogger.classic.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class RecordDTO {

    private Long id;
    private String name;
    private LocalDateTime begin;
    private LocalDateTime end;
    private Long userId;
}
