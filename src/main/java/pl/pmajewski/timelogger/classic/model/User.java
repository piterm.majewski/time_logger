package pl.pmajewski.timelogger.classic.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import pl.pmajewski.timelogger.classic.dto.UserCreateDTO;
import pl.pmajewski.timelogger.classic.dto.UserDTO;
import pl.pmajewski.timelogger.classic.dto.UserUpdateDTO;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@EqualsAndHashCode(of = "id")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    private String password;

    @CreationTimestamp
    private LocalDateTime registerTimestamp;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Record> records;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public void addRecord(Record record) {
        this.records.add(record);
    }

    public void deleteRecord(Record record) {
        records.remove(record);
    }

    public void update(UserUpdateDTO body) {
        email = body.getEmail();
        password = body.getPassword();
    }

    public UserDTO getDTO() {
        return new UserDTO(id, email, registerTimestamp);
    }

    public static User of(UserCreateDTO body) {
        return new User(body.getEmail(), body.getPassword());
    }
}
