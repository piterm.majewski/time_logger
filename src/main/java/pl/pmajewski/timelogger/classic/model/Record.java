package pl.pmajewski.timelogger.classic.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.pmajewski.timelogger.classic.dto.RecordCreateDTO;
import pl.pmajewski.timelogger.classic.dto.RecordDTO;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "records")
@Data
@EqualsAndHashCode(of = "id")
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime begin;

    @Temporal(TemporalType.TIMESTAMP)
    private  LocalDateTime end;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user")
    private User user;

    public Record(User user, RecordCreateDTO body) {
        this.user = user;
        this.name = body.getName();
        this.begin = body.getBegin();
        this.end = body.getEnd();
    }

    public RecordDTO getDTO() {
        return RecordDTO.builder()
                .id(id)
                .begin(begin)
                .end(end)
                .name(name)
                .userId(user.getId())
                .build();
    }
}
